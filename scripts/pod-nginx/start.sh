#!/bin/bash
# Podman container setup script.
# Simply copy this into your project. Add author, name, version, volumes, ports and networks to the
# project configuration below.

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Project configuration
# Example:
# IMAGE_AUTHOR="0x20"
# IMAGE_NAME="apache2"
# IMAGE_VERSION="1.0"
# VOLUMES=(data:/var/www/html)
# PORTS=(80:80/tcp 443:443/tcp)
# NETWORKS=() #TODO

# Image information
IMAGE_AUTHOR="0x20"
IMAGE_NAME="nginx"
IMAGE_VERSION="1.0"
IMAGE_LATEST="$IMAGE_AUTHOR/$IMAGE_NAME:latest"
IMAGE_FULL="$IMAGE_AUTHOR/$IMAGE_NAME:$IMAGE_VERSION"

# List of volumes volumeName:containerPath
VOLUMES=(data:/var/www/html)

# List of exposed ports hostPort:containerPort/scheme
PORTS=(80:80/tcp 443:443/tcp)

# List of attached networks
NETWORKS=()

function main
{
	# Some containers require root privileges (eg. for port binding below 1024)
	require_root true

	# Build latest image
	echo ">>> EXECUTING: podman build $SCRIPT_DIR -t $IMAGE_LATEST -t $IMAGE_FULL"
	podman build "$SCRIPT_DIR" -t "$IMAGE_LATEST" -t "$IMAGE_FULL"

	# Run container detached with specified ports, volumes, networks
	echo ">>> EXECUTING: podman run -d $(build_podman_port_string) $(build_podman_volume_string) --rm --name $IMAGE_NAME-$IMAGE_VERSION $IMAGE_FULL"
	podman run -d $(build_podman_port_string) $(build_podman_volume_string) --rm --name "$IMAGE_NAME-$IMAGE_VERSION" "$IMAGE_FULL"
}

function build_podman_port_string
{
	p=""
	for i in "${PORTS[@]}"; do
		p="$p-p $i "
	done
	echo "$p"
}

function build_podman_volume_string
{
	v=""
	for i in "${VOLUMES[@]}"; do
		v="$v-v $IMAGE_AUTHOR-$IMAGE_NAME-$IMAGE_VERSION-$i "
	done
	echo "$v"
}

function require_root
{
	if [ "$1" == "true" ]; then
		if [ ! "$(whoami)" == "root" ]; then
			echo "You need to be root to execute this script! Aborting..."
			exit 3
		fi
	fi

}

main "$@"
