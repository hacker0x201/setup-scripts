#!/bin/bash
# Installation script template.
# Simply copy this into your project, add supported operating system versions to
# the global arrays and add all the necessary installation steps.
#
# If you want to branch depending on certain operating system versions,
# you can use the following code snippit:
# +-----------------------------------------------+
#	# [OPTIONAL] Version specific branching
#	if [ "$version" == "18.04" ]; then
#		# my stuff for 18.04 here...
#	elif [ "$version" == "16.04" ]; then
#		# my stuff for 16.04 here...
#	fi
# +-----------------------------------------------+
# Simply copy and paste it into one of the installation functions.
#
# Example for supported os version arrays:
# SUPPORTED_DEBIAN_VERSIONS=(10 9 8 7)
# SUPPORTED_UBUNTU_VERSIONS=(19.10 19.04 18.04)
# SUPPORTED_RASPBIAN_VERSIONS=(10 9 8 7)

SUPPORTED_DEBIAN_VERSIONS=()
SUPPORTED_UBUNTU_VERSIONS=()
SUPPORTED_RASPBIAN_VERSIONS=()

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function install_dependencies() {
	# Put your global dependencies like wget/python/php in here
	echo "Installing dependencies..."
}

function install_debian() {
	# Beginning of installation script
	require_root false
	version="$1"
	echo "Installing for Debian $version..."

	# [OPTIONAL] Version specific branching
	# -
}

function install_ubuntu() {
	# Beginning of installation script
	require_root false
	version="$1"
	echo "Installing for Ubuntu $version..."

	# [OPTIONAL] Version specific branching
	# -
}

function install_raspbian() {
	# Beginning of installation script
	require_root false
	version="$1"
	echo "Installing for Raspbian $version..."

	# [OPTIONAL] Version specific branching
	# -
}

function pre_setup_check_and_branch() {
	# Determine current OS, including the version.
	# Branch into the target specific installation script if
	# a supported version has been found.
	source /etc/os-release
	if [ "$ID" == "debian" ]; then
		for i in "${SUPPORTED_DEBIAN_VERSIONS[@]}"; do
			if [ "$i" == "$VERSION_ID" ]; then
				install_dependencies
				install_debian "$i"
				exit 0
			fi
		done
		echo "There is no installation script for $ID with version $VERSION_ID"
		exit 2
	elif [ "$ID" == "ubuntu" ]; then
		for i in "${SUPPORTED_UBUNTU_VERSIONS[@]}"; do
			if [ "$i" == "$VERSION_ID" ]; then
				install_dependencies
				install_ubuntu "$i"
				exit 0
			fi
		done
		echo "There is no installation script for $ID with version $VERSION_ID"
		exit 2
	elif [ "$ID" == "raspbian" ]; then
		for i in "${SUPPORTED_RASPBIAN_VERSIONS[@]}"; do
			if [ "$i" == "$VERSION_ID" ]; then
				install_dependencies
				install_raspbian "$i"
				exit 0
			fi
		done
		echo "There is no installation script for $ID with version $VERSION_ID"
		exit 2
	else
		echo "There is no installation script for $ID"
		exit 1
	fi
}

function require_root() {
	if [ "$1" == "true" ]; then
		if [ ! "$(whoami)" == "root" ]; then
			echo "You need to be root to execute this script! Aborting..."
			exit 3
		fi
	fi

}

pre_setup_check_and_branch "$@"
